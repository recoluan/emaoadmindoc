# 组件

## BackTop（返回顶部）

**接受的参数：**

- **visibilityHeight** (Number) 滚动条滚动多少开始出现返回顶部按钮
- **backPosition** (Number) 返回顶部的位置
- **customStyle** (Object) 自定义按钮的样式
  - right 距离屏幕右边的距离 
  - bottom: 距离屏幕下边的距离 
  - width: 宽度
  - height: 高度
  - border-radius 圆角
  - line-height: 行高
  - background: 背景色

## Breadcrumb（头部的的面包屑菜单）

## Dropzone（Dropzone.js图片上传）

**必填的参数：**

- **id** (String) dom的id
- **url** (String) 上传图片的URL

**触发的方法**

- **dropzone-removedFile** 移除正在上传的图片时触发的方法
- **dropzone-success** 上传成功时出发的方法

[官网](https://www.dropzonejs.com)

```javascript
var myDropzone = new Dropzone("#dropz", {
  url: "/admin/upload",//文件提交地址
  method:"post",  //也可用put
  paramName:"file", //默认为file
  maxFiles:1,//一次性上传的文件数量上限
  maxFilesize: 2, //文件大小，单位：MB
  acceptedFiles: ".jpg,.gif,.png,.jpeg", //上传的类型
  addRemoveLinks:true,
  parallelUploads: 1,//一次上传的文件数量
  //previewsContainer:"#preview",//上传图片的预览窗口
  dictDefaultMessage:'拖动文件至此或者点击上传',
  dictMaxFilesExceeded: "您最多只能上传1个文件！",
  dictResponseError: '文件上传失败!',
  dictInvalidFileType: "文件类型只能是*.jpg,*.gif,*.png,*.jpeg。",
  dictFallbackMessage:"浏览器不受支持",
  dictFileTooBig:"文件过大上传文件最大支持.",
  dictRemoveLinks: "删除",
  dictCancelUpload: "取消",
  init:function(){
    this.on("addedfile", function(file) {
      //上传文件时触发的事件
    });
    this.on("success",function(file,data){
      //上传成功触发的事件
    });
    this.on("error",function (file,data) {
      //上传失败触发的事件
    });
    this.on("removedfile",function(file){
      //删除文件时触发的方法
    });
  }
});
```

## Hamburger（头部汉堡菜单）

## ImageCropper（带裁剪功能的图片上传）

## MDinput（带动画占位符的输入框）

## Screenfull（全屏）

**接受的参数：**

- **width** (Number) 宽度
- **height** (Number) 高度
- **fill** (String) 颜色

## ScrollPane（内容可滚动容器）

## Share/dropdownMenu（炫酷下拉列表）

**接受的参数：**

- **items** (Arrary) 列表内容
  - title 标题
  - href 链接
- **title** (String) 高度

## Sticky（粘粘，同指令v-sticky）

**接受的参数：**

- **items** (Arrary) 列表内容
  - title 标题
  - href 链接
- **title** (String) 高度

## Sticky（粘粘，同指令v-sticky）

**接受的参数：**

- **stickyTop** (Number) 粘粘时距离
- **zIndex** (Number)
- **className** (String) 高度

## ThemePicker（主题颜色切换）