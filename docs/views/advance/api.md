# API

:::tip 
1. 项目的接口按照模块进行分类，例如 **登录** 和 **获取用户信息** 这种的接口放在 `@/api/base.js` ，像 **供应链金融** 的接口都放在 `@/api/supplyChainFinance.js` 中，方便管理和引用；
2. 命名与页面组件一致，例如 **供应链金融** 的页面均放在 `@/views/supplyChainFinance/` 中；
3. 当你开发完新的口以后，一定记得过来更新文档。
:::

## 开发

如果要开发新的模块的接口，需要在/src/api/下新建以模块名为名字的js，如果在旧模块新增接口，只需要找到该模块新增即可。下面以test.js为例：

```javascript
/**
 * 请严格按以下案例来书写接口注释，如果某个参数为选填，外加optional
 */

// 引入 $get 和 $post
import { $get, $post } from '@/utils/fetch.js'

/**
 * 登录
 * @param phone {Sting} 电话号码
 * @param password {Sting} 密码
 */
const login = function (phone, password) {
  return $post('/user/login', {
    data: {
      phone,
      password
    }
  })
}

/**
 * 授信审核
 * @param params {Object} 参数对象，包括：
 *        creditId          {Number} 授信ID
 *        proposeAmount     {Number} 建议授信额度
 *        proposeSingleLoan {Number} 建议单笔放款最高金额
 *        insideOpinion     {String} 内部审批意见
 *        outsideOpinion    {String} 外部审批意见
 *        operation         {Number} 同意或拒绝，同意: 1，拒绝：2
 */
const creditCheck = function (params) {
  return $get('/scf/credit', {
    data: params
  })
}

/**
 * 授信申请查询
 * @param params {Object} 参数对象，包括：
 *        companyName {String, optional} 客户名称
 *        socialCode  {String, optional} 社会信用代码
 *        state       {String, optional} 授信状态 100:拒绝 200:通过 300:待审核
 *        perPage     {Number, optional} 每页显示数
 *        page        {Number, optional} 页码
 *        startTime   {String, optional} 开始时间
 *        endTime     {String, optional} 结束时间
 *        type        {Number, optional} 审核状态 1:待办 2:已办
 */
const creditSearch = function (params) {
  return $get('/scf/credits', {
    data: params
  })
}

/**
 * 资金方列表
 */
const fundParty = function () {
  return $get('/scf/fundParty')
}

// 对外暴露接口
export { login, creditCheck, creditSearch, fundPart }
```


## 引用
在 `@/api/base.js` 中有 **登录** 和 **获取用户信息** 两个接口，如果只想引用登录接口，建议：

```javascript
import { login } from '@/api/base.js'

// 使用login接口
login({
  userName: '',
  password: ''
}).then(res => {})
```

如果想引用base.js中的所有接口，建议：

```javascript
import { base } from '@/api'
 
// 使用login接口
base.login({
  userName: '',
  password: ''
}).then(res => {})
```

## 接口列表

```bash
├── base.js                       // 基础公共接口（file）
│   ├── login                     // 登录
│   └── getUserInfo               // 获取用户信息
└── supplyChainFinance.js         // 供应链金融接口（file）
    ├── creditCheck               // 授信审核
    ├── creditSearch              // 授信申请查询
    ├── creditDetail              //授信申请详情
    ├── fundParty                 // 资金方列表
    ├── quota                     // 创建客户额度
    ├── lineSearch                // 客户额度查询
    ├── creditId                  // 客户额度详情
    └── creditSearch              // 授信申请查询
```