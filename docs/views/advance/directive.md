# 指令

## v-clipboard

功能：复制

```html
<template>
  <div v-clipboard:copy="copyData">赋值按钮</div>
</template>

<script>
export default {,
   data () {
     return {
       copyData: '123'
     }
   }
}
</script>
```

如果需要js来触发复制功能，这里还专门准备了 [**复制方法**](./utils.md#clipboard)

## v-sticky

功能：效果类似于css3属性position:sticky;

```html
<template>
  <div v-sticky>赋值按钮</div>
</template>
```

但是兼容性可能不好，这里还专门准备了 [**粘粘组件**]()

## v-el-drag-dialog

功能：为了让弹窗可拖拽

```html
<el-dialog v-el-drag-dialog></el-dialog>
```

## v-permission

功能：设置查看权限

```html
<span v-permission="['admin']"></span>
```

## v-waves

功能：点击出现水纹动画

```html
    <el-button v-waves></el-button>
```