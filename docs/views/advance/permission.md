# 权限

### 权限分为两种：路由权限和组件权限

## 页面权限

该项目中权限的实现方式是：用户登录后返回该用户可以访问的页面的路由数组，然后将该数组添加到menuMap中，生成当前用可以看到的菜单。

## 组件权限

未完待续。。。

**局限**

在某些情况下，不适合使用v-permission，例如元素Tab组件，只能通过手动设置v-if来实现。

可以使用全局权限判断函数，用法和指令 `v-permission` 类似。

```html
<template>
  <el-tab-pane v-if="checkPermission(['admin'])" label="Admin">Admin can see this</el-tab-pane>
  <el-tab-pane v-if="checkPermission(['editor'])" label="Editor">Editor can see this</el-tab-pane>
  <el-tab-pane v-if="checkPermission(['admin','editor'])" label="Admin-OR-Editor">Both admin or editor can see this</el-tab-pane>
</template>

<script>
import checkPermission from '@/utils/permission' // 权限判断函数

export default{
   methods: {
    checkPermission
   }
}
</script>
```
