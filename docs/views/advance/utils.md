# 方法

## auth

设置和获取token的缓存

## clipboard

复制方法，功能同[复制指令](./filter.md#v-clipboard)

## createUniqueString

获取独特字符串，可以用作唯一标志

## fetch

fetch封装

## getVerson

将 `1.1.1`  这样的版本号转化成 `StoneOneI` ，转换规则

## handledata

处理数据

## toFixed

小数计算去精度

## validate

验证（正则）

## index

): 天啊，太多了，先让我整理其他的