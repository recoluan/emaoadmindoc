# emaoUI

## 卡片

**效果**

<img src="./images/emao-card.jpg" />

**使用**

```html
<!-- 通过icon和title自定义标题内容和图标 -->
<emao-card icon="icon-home" title="数据列表">
  <div slot="header-right">
    <!-- 标题右侧自定义内容 -->
    <el-button type="primary">导出表格</el-button>
  </div>
  <!-- 内容 -->
</emao-card>
```

**参数**

1. icon (String) 图标
2. title (String) 标题
3. header-right (slot) 自定义标题栏右侧内容

## 屏幕自适应的多栏布局

**效果**

<img src="./images/emao-grid-item.jpg" />

**使用**

```html
<!-- 通过part自定义占比的分母n -->
<emao-grid-item :part="2" class="screen-item-wrapper flex-wrapper">
  <!-- 内容 -->
</emao-grid-item>
```

**参数**

1. part (Number) 自定义占比

## 分页

**效果**

<img src="./images/emao-pagination.jpg" />

**使用**

```html
<emao-pagination 
  :total="total" 
  :current-page="currentPage" 
  @getCurrentPage="getCurrentPage"
  @getPerPage="getPerPage"></emao-pagination>
```

**参数**

1. total (Number) 总共多少条数据
2. current-page (Number) 当前页面
3. getCurrentPage (Function) 切换页面后，获取当前是第几页
4. getPerPage (Function) 切换每夜展示的条数时，获取当前每页展示多少条数据，默认是10

## 条件筛选

::: tip
根据  [emao-card](#卡片)二次开发，配合[emao-screen-item](#条件筛选子元素) 使用
:::

**效果**

<img src="./images/emao-screen-wrapper.jpg" />

**使用**

```html
<emao-screen-wrapper @search="search">
  <emao-screen-item label="客户名称">
    <el-input v-model="input" placeholder="请输入内容"></el-input>
  </emao-screen-item>
  <emao-screen-item label="社会信用代码">
    <el-input v-model="input" placeholder="请输入内容"></el-input>
  </emao-screen-item>
</emao-screen-wrapper>
```

**参数**
1. search (Function) 点击查询按钮触发的函数

## 条件筛选子元素

::: tip
根据 [emao-grid-item](#屏幕自适应的多栏布局) 二次开发，配合 [emao-screen-wrapper](#条件筛选) 使用
:::

**效果**

<img src="./images/emao-screen-item.jpg" />

**使用**

```html
<emao-screen-item label="社会信用代码">
  <el-input v-model="input" placeholder="请输入内容"></el-input>
</emao-screen-item>
```

**参数**
1. label (String) 左侧label文案
2. part (Number) 自定义占比

## 特殊两行表单布局

**效果**

美化前

<img src="./images/emao-two-line-old.jpg" />

美化后

<img src="./images/emao-two-line.jpg" />

**使用**

```html
<!-- 在外层加一个two-line-form类即可 -->
<emao-card title="授信申请信息" class="two-line-form">
  <el-row class="padding20">
    <emao-screen-item label="授信申请编号" :part="part">
      <el-input v-model="input" placeholder="请输入内容"></el-input>
    </emao-screen-item>
    <emao-screen-item label="授信申请时间" :part="part">
      <el-input v-model="input" placeholder="请输入内容"></el-input>
    </emao-screen-item>
  </el-row>
</emao-card>
```

## 多选表格

**效果**

<img src="./images/emao-multi-select-table.png" />

**使用**

```html
<!-- 表格 -->
<template>
  <emao-card icon="icon-home" title="数据列表" class="table-wrapper">
    <div slot="header-right">
      <el-button type="primary">导出表格</el-button>
    </div>
    <el-table
      ref="multipleTable"
      :data="tableData"
      tooltip-effect="dark"
      :border="true"
      style="width: 100%"
      @select="singleSelectionChange"
      @select-all="allSelectionChange(tableData)">
      <el-table-column type="selection" width="40"></el-table-column>
      
    </el-table>
  </emao-card>
</template>

<script>
import multiSelect from '@/mixins/multiSelectTable.js'

export default {
  mixins: [multiSelect],
  data () {
    return {
      tableData: []
    }
  },
  methods: {
    // 获取当前页码
    getCurrentPage (currentPage) {
      getDataPromise().then(res => {
        this.$nextTick(() => {
          this._restoreSelected(this.tableData, 'multipleTable', 'id')
        })
      })
    },
    // 表格单个选择
    singleSelectionChange (selection, row) {
      this._dealSeclect(row, 'id')
    },
    // 表格全选
    allSelectionChange (tableData) {
      this._dealSeclect(tableData, 'id')
    }
  }
}  
</script>
```

## 预览功能

> 预览组件只需要全局引用一次

**使用**

```html
<!-- 表格 -->
<template>
  <div @click="openPreview">打开预览</div>
</template>

<script>
export default {
  methods: {
    openPreview () {
      this.$showPreview({
        // 是否显示预览
        isShow: true,
        // 1图片 2视频 3音频 4PDF 5office文件
        fileType: 1,
        // 接受字符串、数组和json对象
        previewData: [
          'https://ss1.baidu.com/6ONXsjip0QIZ8tyhnq/it/u=282609871,2046421540&fm=58&bpow=2048&bpoh=2745',
          'http://seopic.699pic.com/photo/50055/5642.jpg_wh1200.jpg',
        ]
      })
    }
  }
}  
</script>
```
