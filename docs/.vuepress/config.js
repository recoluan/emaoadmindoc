module.exports = {
  base: '/emaoadmindoc/',
  title: 'eMao-Admin-Doc',
  description: 'We are emao front-end, this is a new start.',
  dest: 'public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }]
  ],
  themeConfig: {
    nav: [
      { text: '首页', link: '/' },
      { text: '指南', link: '/views/basic/' },
      { text: '高级', link: '/views/advance/' },
      { text: '其他', 
        items: [
          { text: 'Vuex精简', link: 'https://recoluan.gitlab.io/views/frontEnd/2018/091001.html' },
          { text: 'fetch封装', link: 'https://recoluan.gitlab.io/views/frontEnd/2018/091301.html' },
        ]
      },
      { text: 'GitHub', link: 'https://gitlab.com/recoluan/emaoadmindoc' },
    ],
    sidebar: {
      '/views/basic/': [
        {
          title: '指南',
          collapsable: false,
          children: [
            '',
            'emaoUI'
          ]
        }
      ],
      '/views/advance/': [
        {
          title: '高级',
          collapsable: false,
          children: [
            '',
            'api',
            'router',
            'filter',
            'permission',
            'directive',
            'component',
            'mixin',
            'utils',
            'icon',
            'style',
          ]
        },
        {
          title: '进阶',
          collapsable: false,
          children: [
            'vuex',
            'plugin',
            'theme',
            'mock',
          ]
        },
        {
          title: '其他',
          collapsable: false,
          children: [
            'other',
            'version'
          ]
        }
      ],
      '/': [
        ''
      ]
    }
  }
}