---
home: true
heroImage: /hero.png
actionText: Guide →
actionLink: /views/basic/
features:
- title: 指南
  details: 方便公司开发者快速开发页面。无论你是否拥有web前端基础，根据文档均可通过copy快速生成页面及路由。
- title: 高级
  details: 为了公司的前端开发者深入而提供的技术文档，例如公共状态管理、权限、切换主题色、图标等。
- title: Fetch封装    
  details: 使用fetch代替传统的ajax
footer: by emao front-end
---